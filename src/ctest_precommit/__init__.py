import argparse
import subprocess
import pathlib

import os

def main():
    parser = argparse.ArgumentParser(
            prog="ctest-precommit",
            description="pre-commit hook to build and run tests with cmake/ctest",
        )
    parser.add_argument("--build-dir", default="build")
    parser.add_argument("--target", default="all")

    args = parser.parse_args()

    pathlib.Path(args.build_dir).mkdir(parents=True, exist_ok=True)

    result = subprocess.run(
            ["cmake", "-B", args.build_dir, "."],
            capture_output=True,
            text=True
        )
    if result.returncode != 0:
        print(f"error running cmake:---------------------")
        print(f"stdout:----------------------")
        print(result.stdout)
        print(f"stderr:---------------------")
        print(result.stderr)
        print("------------------------------")
        exit(result.returncode)

    result = subprocess.run(
            ["make", "-C", args.build_dir, args.target],
            capture_output=True,
            text=True
        )
    if result.returncode != 0:
        print(f"error running make:---------------------")
        print(f"stdout:----------------------")
        print(result.stdout)
        print(f"stderr:---------------------")
        print(result.stderr)
        print("------------------------------")
        exit(result.returncode)

    result = subprocess.run(
            ["ctest", "--test-dir", args.build_dir],
            capture_output=True,
            text=True
        )
    if result.returncode != 0:
        print(f"error running ctest:---------------------")
        print(f"stdout:----------------------")
        print(result.stdout)
        print(f"stderr:---------------------")
        print(result.stderr)
        print("------------------------------")
        exit(result.returncode)


